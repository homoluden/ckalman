#include <stdio.h>
#include <math.h>

#include "filters_impl.h"
#include "USTAB_AHRS\uahrs.h"

#define C_API __declspec(dllexport)

/* Exported Method Prototypes */
C_API void Stage0_SeedParams(s64* params39, u8 idx);
C_API void Stage1_OptimizeGyro(s16* inputs, u32 rows, u32 cols, s64* params39);
C_API void KF6_3_Init(s64* params39, u8 idx);
C_API void UAHRS_Init(s16* initAcc3, s16* initMag3, s64* accPid3, double dt);
C_API void KF6_3_Step(s16* inpInt, double* est, u8 idx);
C_API void UAHRS_Step(double* gyro, double* acc, double* eulerResult);


static double paramScale = 1e-5f;

static double a[4] = {
    0.5309f,    -33.26f,
    0.001497f,    0.9633f
};

static double c[2] = { 166.7f,  2.222e+04f };

static double mCov[9] = {
    1.0000f,    0.0000f,    0.0000f,
    0.0000f,	1.0000f,    0.0000f,
    0.0000f,	0.0000f,    1.0000f,
};

static double pCov[36] = {
    1e-5f  ,    0.0000f,    0.0000f,    0.0000f,    0.0000f,    0.0000f,
    0.0000f,    1e-5f  ,    0.0000f,    0.0000f,    0.0000f,    0.0000f,
    0.0000f,    0.0000f,    1e-5f  ,    0.0000f,    0.0000f,    0.0000f,
    0.0000f,    0.0000f,    0.0000f,    1e-5f  ,    0.0000f,    0.0000f,
    0.0000f,    0.0000f,    0.0000f,    0.0000f,    1e-5f  ,    0.0000f,
    0.0000f,    0.0000f,    0.0000f,    0.0000f,    0.0000f,    1e-5f  ,
};

//var gyroScale = 250.0 / 32768.0;
//var accelScale = 2.0 / 32768.0;
//var magScale = 10.0 * 4219.0 / 32768.0;

//static double gyroDrift[3] = { -0.18282532f, -0.074943456f, -0.317776303f};
static double gyroDrift[3] = { -0.2f, -0.1f, -0.4f };
static double gyroScale[3] = { 250.0 / 32768.0, 250.0 / 32768.0, 250.0 / 32768.0 };

static double accDrift[3] = {0.0f, 0.0f, 0.0f};
static double accScale[3] = { 2.0 / 32768.0, 2.0 / 32768.0, 2.0 / 32768.0 };

static double magDrift[3] = {0.0f, 0.0f, 0.0f};
static double magScale[3] = { 10.0 * 4219.0 / 32768.0, 10.0 * 4219.0 / 32768.0, 10.0 * 4219.0 / 32768.0 };

#define GYRO_KF 0
#define ACC_KF 1
#define MAG_KF 2
#define STEP_MAX 1000
#define a_START 0
#define c_START 4
#define drft_START 6
#define scl_START 9
#define mCov_START 12
#define pCov_START 18

void Stage0_SeedParams(s64* params39, u8 idx)
{

    params39[a_START] = (s64)(a[0] / paramScale); params39[a_START + 1] = (s64)(a[1] / paramScale); 
    params39[a_START + 2] = (s64)(a[2] / paramScale); params39[a_START + 3] = (s64)(a[3] / paramScale);
    params39[c_START] = (s64)(c[0] / paramScale); params39[c_START + 1] = (s64)(c[1] / paramScale);

    double *drift, *scale;

    if (idx == GYRO_KF)
    {
        drift = gyroDrift;
        scale = gyroScale;
    }
    else if (idx == ACC_KF)
    {
        drift = accDrift;
        scale = accScale;
    }
    else if (idx == MAG_KF)
    {
        drift = magDrift;
        scale = magScale;
    }

    if (idx == GYRO_KF || idx == ACC_KF || idx == MAG_KF)
    {
        params39[drft_START] = (s64)(drift[0] / paramScale); params39[drft_START + 1] = (s64)(drift[1] / paramScale); params39[drft_START + 2] = (s64)(drift[2] / paramScale);
        params39[scl_START] = (s64)(scale[0] / paramScale); params39[scl_START + 1] = (s64)(scale[1] / paramScale); params39[scl_START + 2] = (s64)(scale[2] / paramScale);
    }

    u32 k = 0;
    for (u32 i = 0; i < 3; i++) { for (u32 j = i; j < 3; j++, k++) { params39[mCov_START + k] = (s64)(mCov[i * 3 + j] / paramScale); } }

    k = 0;
    for (u32 i = 0; i < 6; i++) { for (u32 j = i; j < 6; j++, k++) { params39[pCov_START + k] = (s64)(pCov[i * 6 + j] / paramScale); } }

}

double AnglesDiffDegree(double a, double b)
{
    if (a < 0.0)
    {
        a += 360.0;
    }

    if (b < 0.0)
    {
        b += 360.0;
    }

    double fwdDiff = a - b;
    double absDiff = fabs(fwdDiff);

    return absDiff < 180.0 ? fwdDiff : (a < b ? 360.0 - absDiff : absDiff - 360.0);
}

double Stage1_TestGyro(s16* inputs, u32 rows, u32 cols)
{
    double euler0[3] = { 0.0f, };
    double euler1[3] = { 0.0f, };
    double dx, dy, dz;
    u32 i;
    for (i = 0; i < rows; i++)
    {
        u32 row = i * cols;
        s16 *rowPointer = (s16*)(inputs + row + 1);
        double w[3] = { 0.0, };
        short wInt[] = { rowPointer[0], rowPointer[1], rowPointer[2] };
        KF6_3_Step(wInt, w, 0);
        if (i > 700)
        {
            uahrs_update(w);
        }
        


        // Assuming that first 100 peasurements Gyro was in default position w/o movemetn.
        // Also, assuming that KF now is warmed up properly.
        if (i == 701)
        {
            uahrs_getEulerAngles(euler0);
        }

        if (i % 100 == 0)
        {
            uahrs_getEulerAngles(euler1);
            i += 0;
        }
        
    }

    uahrs_getEulerAngles(euler1);
    
    dx = AnglesDiffDegree(euler0[0], euler1[0]);
    dy = AnglesDiffDegree(euler0[1], euler1[1]);
    dz = AnglesDiffDegree(euler0[2], euler1[2]);
    
    return 0.33 * (dx*dx + dy*dy + dz*dz);
}

void GradNormalize(double *grad3)
{
    float t = FastInvSqrt(grad3[0] * grad3[0] + grad3[1] * grad3[1] + grad3[2] * grad3[2]);

    grad3[0] *= t;
    grad3[1] *= t;
    grad3[2] *= t;
}

void Stage1_GetDriftGradient(s64* params, s16* inputs, u32 rows, u32 cols, double* out_grad)
{
    // Score 0 - score of not modified (current) params
    // Score 1..3 - Score of Drift param modifications
    // Score 4..6 - Score of Scale param modifications
    double scores[4] = { 0, };
    u8 delta = 1;

    KF6_3_Init(params, 0);

    scores[0] = Stage1_TestGyro(inputs, rows, cols);

    params[drft_START] += delta;
    KF6_3_Init(params, 0);
    scores[1] = Stage1_TestGyro(inputs, rows, cols);
    params[drft_START] -= delta;

    params[drft_START + 1] += delta;
    KF6_3_Init(params, 0);
    scores[2] = Stage1_TestGyro(inputs, rows, cols);
    params[drft_START + 1] -= delta;

    params[drft_START + 2] += delta;
    KF6_3_Init(params, 0);
    scores[3] = Stage1_TestGyro(inputs, rows, cols);
    params[drft_START + 2] -= delta;

    out_grad[0] = scores[0];
    /*scores[1] /= scores[0];
    scores[2] /= scores[0];
    scores[3] /= scores[0];*/
    out_grad[1] = scores[1] - scores[0]; out_grad[2] = scores[2] - scores[0]; out_grad[3] = scores[3] - scores[0];

    GradNormalize((double *)(out_grad + 1));
}

void Stage1_UpdateGyroParams(s64* params, double* grad, u16 step)
{
    u32 j = 1;
    for (u32 i = drft_START; i < drft_START+3; i++, j++)
    {
        params[i] -= (s64)floor(0.1 * step * grad[j]);
    }
}

C_API void Stage1_OptimizeGyro(s16* inputs, u32 rows, u32 cols, s64* params39)
{
    // Step value for Gradient Descent alg.
    // Will multiply the Gradient calculated as Score Difference for each param incrementation.
    // Also it will be decremented downto min value each iteration of Grad. Descent alg.
    u16 step = STEP_MAX;

    double grad[4] = { 0, };
    /*  Params Initialization  */
    s64 *params = params39;

    Stage0_SeedParams(params, 0);

    // Using fixed iterations count for first version of Grad Descent
    for (u32 i = 0; i < STEP_MAX-1; i += 1)
    {
        Stage1_GetDriftGradient(params, inputs, rows, cols, grad);

        Stage1_UpdateGyroParams(params, grad, step);
        if (step > 10) { step -= 1; }
    }
}

/// Params:
/// params39 : int[4 + 2 + 3  +  3  +  6  +  21]
///                a   c  drft  scal  mCov  pCov
/// This method converts the flat array of integer quazi-parameters into real-type parameters in algorithm.
/// This conversion is needed for convenience to make it possible to use this method in optimization routines, e.g. Genetic Algorithm or Gradient Descent
C_API void KF6_3_Init(s64* params39, u8 idx)
{
	a[0] = params39[a_START] * paramScale; a[1] = params39[a_START+1] * paramScale; a[2] = params39[a_START+2] * paramScale; a[3] = params39[a_START+3] * paramScale;
	c[0] = params39[c_START] * paramScale; c[1] = params39[c_START+1] * paramScale;

    double *drift, *scale;

    if (idx == GYRO_KF)
    {
        drift = gyroDrift;
        scale = gyroScale;
    }
    else if (idx == ACC_KF)
    {
        drift = accDrift;
        scale = accScale;
    }
    else if (idx == MAG_KF)
    {
        drift = magDrift;
        scale = magScale;
    }

    if (idx == GYRO_KF || idx == ACC_KF || idx == MAG_KF)
    {
        drift[0] = params39[drft_START] * paramScale; drift[1] = params39[drft_START + 1] * paramScale; drift[2] = params39[drft_START + 2] * paramScale;
        scale[0] = params39[scl_START] * paramScale; scale[1] = params39[scl_START + 1] * paramScale; scale[2] = params39[scl_START + 2] * paramScale;
    }

	u32 k = 0;

	//mCov[0] = params33[6]; mCov[1] = mCov[3] = params33[7]; mCov[2] = mCov[6] = params33[8]; mCov[4] = params33[9]; mCov[5] = mCov[7] = params33[10]; mCov[8] = params33[11];
	for (u32 i = 0; i < 3; i++)
	{
		for (u32 j = i; j < 3; j++, k++)
		{
			mCov[i * 3 + j] = pCov[j * 3 + i] = params39[mCov_START + k] * paramScale;
		}
	}

	k = 0;
	for (u32 i = 0; i < 6; i++)
	{
		for (u32 j = i; j < 6; j++, k++)
		{
			pCov[i * 6 + j] = pCov[j * 6 + i] = params39[pCov_START + k] * paramScale;
		}
	}

    kf6_3_init(a, c, mCov, pCov, drift, scale, idx);
}

C_API void UAHRS_Init(s16* initAcc3, s16* initMag3, s64* accPid3, double dt)
{
    double accPid[3] = { accPid3[0] * paramScale, accPid3[1] * paramScale, accPid3[2] * paramScale };

    uahrs_init(accPid, dt);
    setInitState_byAccMag(initAcc3, initMag3);
}

C_API void KF6_3_Step(s16* inpInt, double* est, u8 idx)
{
    kf6_3_step(inpInt, est, idx);
}

C_API void UAHRS_Step(double* gyro, double* acc, double* eulerResult)
{
    double euler[3] = { 0.0f, };
	double w[3] = { gyro[0], gyro[1], gyro[2]};
    uahrs_update(w);
    //uahrs_MahonyAHRSupdateIMU(w[0], w[1], w[2], acc[0], acc[1], acc[2]);

    uahrs_getEulerAngles(euler);
    eulerResult[0] = euler[0];
    eulerResult[1] = euler[1];
    eulerResult[2] = euler[2];
}

