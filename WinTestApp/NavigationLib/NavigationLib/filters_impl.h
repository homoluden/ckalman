#ifndef __FILTERS_IMPL_H
#define __FILTERS_IMPL_H

#include "typedef.h"
#include "kalman.h"

extern KFParams _kf6_3Params[3];
extern KFState _kf6_3State[3];

extern void kf6_3_init(double* a22, double* c2, double* mCov33, double* pCov66, double* drift3, double* scale3, u8 idx);
extern void kf6_3_step(s16* inp, double* est, u8 idx);

#endif