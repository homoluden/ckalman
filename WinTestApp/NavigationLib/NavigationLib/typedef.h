#ifndef __TYPEDEF_H
#define __TYPEDEF_H

#define u8 unsigned char
#define u16 unsigned short
#define s8 signed char
#define s16 signed short
#define u32 unsigned int
#define s32 signed __int32
#define s64 signed __int64
#define u64 unsigned __int64
#define fp32 float

typedef struct {
  u16 T;
  s16 data[3];
} s16_MEAS3;

#endif