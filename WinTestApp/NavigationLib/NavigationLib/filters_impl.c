#include "filters_impl.h"

extern void kf6_3_init(double* a22, double* c2, double* mCov33, double* pCov66, double* drift3, double* scale3, u8 idx)
{
	/*double a[2][2] = {
		{1.0000,    0.0007 },
		{ -0.0141,    -0.5547 },
	};

	double c[2] = {3000.0, 300.1};*/

	_kf6_3Params[idx] = (KFParams) {
		.F = m_get(6,6),
		.Ft = m_get(6,6),
		.H = m_get(3, 6),
		.Ht = m_get(6, 3),
		.e = m_get(6,6),
		.mCov = m_get(3,3),
		.pCov = m_get(6,6),
		.MeasDrift = v_get(3),
		.MeasScale = v_get(3),
	};

	_kf6_3Params[idx].e = m_ident(_kf6_3Params[idx].e);

	// Setting F and H values
	for (int i = 0; i < 2; i++)
	{
		for (int j = 0; j < 2; j++)
		{
			m_set_val(_kf6_3Params[idx].F, i,   j,   a22[2 * i + j]);
			m_set_val(_kf6_3Params[idx].F, i+2, j+2, a22[2 * i + j]);
			m_set_val(_kf6_3Params[idx].F, i+4, j+4, a22[2 * i + j]);
		}

		m_set_val(_kf6_3Params[idx].H, 0, i,   c2[i]);
		m_set_val(_kf6_3Params[idx].H, 1, i+2, c2[i]);
		m_set_val(_kf6_3Params[idx].H, 2, i+4, c2[i]);

	}

	// Setting mCov values
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			m_set_val(_kf6_3Params[idx].mCov, i,   j,   mCov33[3 * i + j]);
		}
	}

	// Setting pCov values
	for (int i = 0; i < 6; i++)
	{
		for (int j = 0; j < 6; j++)
		{
			m_set_val(_kf6_3Params[idx].pCov, i,   j,   pCov66[6 * i + j]);
		}
	}

	// Setting Drift and Scale values
	for (int i = 0; i < 3; i++)
	{
		v_set_val(_kf6_3Params[idx].MeasDrift, i, drift3[i]);
		v_set_val(_kf6_3Params[idx].MeasScale, i, scale3[i]);
	}

	_kf6_3Params[idx].Ft = m_transp(_kf6_3Params[idx].F, _kf6_3Params[idx].Ft);
	_kf6_3Params[idx].Ht = m_transp(_kf6_3Params[idx].H, _kf6_3Params[idx].Ht);

	_kf6_3State[idx] = (KFState) {
		.X = v_get(6),
		.Z = v_get(3),
		.Zest = v_get(3),
		.P = m_get(6,6)
	};

	_kf6_3State[idx].P = m_copy(_kf6_3Params[idx].pCov, _kf6_3State[idx].P);
	_kf6_3State[idx].X = v_zero(_kf6_3State[idx].X);
}

extern void kf6_3_step(s16* inp, double* est, u8 idx)
{
	v_set_val(_kf6_3State[idx].Z, 0, inp[0] * v_get_val(_kf6_3Params[idx].MeasScale, 0) - v_get_val(_kf6_3Params[idx].MeasDrift, 0));
	v_set_val(_kf6_3State[idx].Z, 1, inp[1] * v_get_val(_kf6_3Params[idx].MeasScale, 1) - v_get_val(_kf6_3Params[idx].MeasDrift, 1));
	v_set_val(_kf6_3State[idx].Z, 2, inp[2] * v_get_val(_kf6_3Params[idx].MeasScale, 2) - v_get_val(_kf6_3Params[idx].MeasDrift, 2));

	v_zero(_kf6_3State[idx].Zest);

	KF_Step(&_kf6_3Params[idx], &_kf6_3State[idx]);

	est[0] = v_get_val(_kf6_3State[idx].Zest, 0);
	est[1] = v_get_val(_kf6_3State[idx].Zest, 1);
	est[2] = v_get_val(_kf6_3State[idx].Zest, 2);
}