/*
 * GenericDCM.h
 *
 *  Created on: 15.06.2013
 *      Author: rfatkhullin
 */

#ifndef GENERICQUAT_H_
#define GENERICQUAT_H_

//#include "../globalparams.h"
#include "../CONTROL/GenericPID3D.h"
#include <math.h>
#include "parametr.h"
#include "utilities.h"

// MahonyAHRS
#define sampleFreq	512.0f			// sample frequency in Hz
#define twoKpDef	(2.0f * 0.5f)	// 2 * proportional gain
#define twoKiDef	(2.0f * 0.0f)	// 2 * integral gain

// MadgwickAHRS
#define betaDef		0.1f		// 2 * proportional gain



class GenericQuat {
public:
	GenericQuat();
	virtual ~GenericQuat();

	void getEulerAngles(float* euler_angles);
	void getDCM();

	float getRotationAngle();
	float* getRotationAxe();

	void setInitState(float* euler_angles);
	void setInitState(float* acc, float* mag);

	void update(float* w, float dt);

	void getEverything(float targetPitchPos, float targetRollPos, float targetYawPos, float t0);

  bool   InitState;
	// MadgwickAHRS
	float beta;				// algorithm gain
	void MadgwickAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az);

	// MahonyAHRS
	float twoKp;			// 2 * proportional gain (Kp)
	float twoKi;			// 2 * integral gain (Ki)
	float integralFBx, integralFBy, integralFBz;	// integral error terms scaled by Ki
	void MahonyAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az, float dt);

	float q0, q1, q2, q3;
public:
	
	float time;
	float _prev_q0, _prev_q1, _prev_q2, _prev_q3;
	
	// �������� DCM �������
	float _e00, _e01, _e02;
	float _e10, _e11, _e12;
	float _e20, _e21, _e22;

	GenericPID3D accPID;
	GenericPID3D magPID;
	GenericPID3D gpsPID;

	float rotationAngle;
	float rotationAxe[3];

	float t_pitch, t_roll, t_yaw;

};

#endif /* GENERICDCM_H_ */
//#define ROLL       0
//#define PITCH      1
//#define YAW        2
