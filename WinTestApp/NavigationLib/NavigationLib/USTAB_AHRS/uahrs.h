#ifndef __UAHRS_H_
#define __UAHRS_H_

#include "../typedef.h"

#define PITCH 0
#define ROLL 1
#define YAW 2

extern double degrees2rads;
extern double rads2degrees;


// MahonyAHRS
#define sampleFreq	512.0f			// sample frequency in Hz
#define twoKpDef	(2.0f * 0.5f)	// 2 * proportional gain
#define twoKiDef	(2.0f * 0.0f)	// 2 * integral gain

// MadgwickAHRS
#define betaDef		0.1f		// 2 * proportional gain

void uahrs_init(double* accPid3, double dt);

void setInitState_byAngles(double* euler_angles);
void setInitState_byAccMag(s16* acc, s16* mag);

// MahonyAHRS
double acc2Kp;			// 2 * proportional gain (Kp)
double acc2Ki;			// 2 * integral gain (Ki)
double acc2Kd;			// 2 * differential gain (Kd)
double integralFBx, integralFBy, integralFBz;	// integral error terms scaled by Ki
void uahrs_MahonyAHRSupdateIMU(double gx, double gy, double gz, double ax, double ay, double az);

void uahrs_getEverything(double targetPitchPos, double targetRollPos, double targetYawPos, double t0);

void uahrs_getEulerAngles(double* euler_angles);
void uahrs_getDCM();
double uahrs_getRotationAngle();
double* uahrs_getRotationAxis();

void uahrs_update(double* w);

fp32 FastInvSqrt(fp32 x);

extern double q0, q1, q2, q3;

double time;
double _prev_q0, _prev_q1, _prev_q2, _prev_q3;

// �������� DCM �������
double _e00, _e01, _e02;
double _e10, _e11, _e12;
double _e20, _e21, _e22;

//GenericPID3D accPID;
//GenericPID3D magPID;
//GenericPID3D gpsPID;

double rotationAngle;
double rotationAxis[3];

extern double t_pitch, t_roll, t_yaw;

extern u8 InitState;

#endif