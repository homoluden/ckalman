/*
 * GenericQuat.cpp
 *
 *  Created on: 15.06.2013
 *      Author: rfatkhullin
 */

#include "GenericQuat.h"

extern int16_t heading;
extern float   f_debug[9];

GenericQuat::GenericQuat() {
	q0 = 1;
	q1 = q2 = q3 = 0;
	
	rotationAngle = 0.0;
	rotationAxe[0] = 0.0;
	rotationAxe[1] = 0.0;
	rotationAxe[2] = 0.0;
	
	
	_prev_q0 = 1;
	_prev_q1 = _prev_q2 = _prev_q3 = 0;
	
  accPID.P=0.1;
	accPID.I=0;
	accPID.D=0;
	
  magPID.P=1.0;
	magPID.I=0;
	magPID.D=0;
	InitState=0;
	
	integralFBx = 0.0f;
	integralFBy = 0.0f;
	integralFBz = 0.0f;
	
	twoKp = 0.5;			// 2 * proportional gain (Kp)
	twoKi = 0.1;			// 2 * integral gain (Ki)	
}

GenericQuat::~GenericQuat() {
}


void GenericQuat::getEulerAngles(float* euler_angles) {
	euler_angles[PITCH] = atan2(2*(q0*q1+q2*q3),q0*q0-q1*q1-q2*q2+q3*q3);
	euler_angles[ROLL] = asin(2*q0*q2-2*q1*q3);
	euler_angles[YAW] = atan2(2*(q0*q3+q1*q2),q0*q0+q1*q1-q2*q2-q3*q3);
}

void GenericQuat::setInitState(float* euler_angles)	{
	float cos_pitch = cos(euler_angles[PITCH]*0.5);
	float sin_pitch = sin(euler_angles[PITCH]*0.5);
	float cos_roll = cos(euler_angles[ROLL]*0.5);
	float sin_roll = sin(euler_angles[ROLL]*0.5);
	float cos_yaw = cos(euler_angles[YAW]*0.5);
	float sin_yaw = sin(euler_angles[YAW]*0.5);

	q0 = cos_pitch*cos_roll*cos_yaw+sin_pitch*sin_roll*sin_yaw;	
	q1 = sin_pitch*cos_roll*cos_yaw-cos_pitch*sin_roll*sin_yaw;	
	q2 = cos_pitch*sin_roll*cos_yaw+sin_pitch*cos_roll*sin_yaw;	
	q3 = cos_pitch*cos_roll*sin_yaw-sin_pitch*sin_roll*cos_yaw;
}

void GenericQuat::setInitState(float* acc, float* mag) {
	float angles[3];
	float t = inverseSqrt(acc[0]*acc[0]+acc[1]*acc[1]+acc[2]*acc[2]);
	float x, y, z;
	x = -acc[0]*t;
	y = -acc[1]*t;
	z = -acc[2]*t;
	angles[PITCH] =atan2(y,z);
	angles[ROLL] = atan2(-x,sqrt(y*y+z*z));
	
  if(mag){
		t = inverseSqrt(mag[0]*mag[0]+mag[1]*mag[1]+mag[2]*mag[2]);
		x = mag[ROLL]*t;
		y = mag[PITCH]*t;
		z = mag[YAW]*t;

		float X = y*cos(-angles[PITCH])+x*sin(-angles[PITCH])*sin(angles[ROLL])+z*sin(-angles[PITCH])*cos(angles[ROLL]);
		float Y =((x*cos(angles[ROLL]))-z*sin(angles[ROLL]));
		angles[YAW] = atan2(Y,X);
	} else 
		angles[YAW]=0;
	setInitState(angles);
	InitState=1;
}

void GenericQuat::getDCM() {
	_e00 = q0*q0+q1*q1-q2*q2-q3*q3;
	_e01 = 2.0*(q1*q2-q0*q3);
	_e02 = 2.0*(q1*q3+q0*q2);
	_e10 = 2.0*(q1*q2+q0*q3);
	_e11 = q0*q0-q1*q1+q2*q2-q3*q3;
	_e12 = 2.0*(q2*q3-q0*q1);
	_e20 = 2.0*(q1*q3-q0*q2);
	_e21 = 2.0*(q2*q3+q0*q1);
	_e22 = q0*q0-q1*q1-q2*q2+q3*q3;
}

void GenericQuat::MahonyAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az, float dt) {
	float recipNorm;
	float halfvx, halfvy, halfvz;
	float halfex, halfey, halfez;
	float qa, qb, qc;

	// Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
	if(!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f))) {

		// Normalise accelerometer measurement
		recipNorm = inverseSqrt(ax * ax + ay * ay + az * az);
		ax *= recipNorm;
		ay *= recipNorm;
		az *= recipNorm;        

		// Estimated direction of gravity and vector perpendicular to magnetic flux
		halfvx = q1 * q3 - q0 * q2;
		halfvy = q0 * q1 + q2 * q3;
		halfvz = q0 * q0 - 0.5f + q3 * q3;
	
		// Error is sum of cross product between estimated and measured direction of gravity
		halfex = (ay * halfvz - az * halfvy);
		halfey = (az * halfvx - ax * halfvz);
		halfez = (ax * halfvy - ay * halfvx);

		// Compute and apply integral feedback if enabled
		if(twoKi > 0.0f) {
//			integralFBx += twoKi * halfex * (1.0f / sampleFreq);	// integral error scaled by Ki
//			integralFBy += twoKi * halfey * (1.0f / sampleFreq);
//			integralFBz += twoKi * halfez * (1.0f / sampleFreq);
			integralFBx += twoKi * halfex * dt;	// integral error scaled by Ki
			integralFBy += twoKi * halfey * dt;
			integralFBz += twoKi * halfez * dt;

			gx += integralFBx;	// apply integral feedback
			gy += integralFBy;
			gz += integralFBz;
		}
		else {
			integralFBx = 0.0f;	// prevent integral windup
			integralFBy = 0.0f;
			integralFBz = 0.0f;
		}

		// Apply proportional feedback
		gx += twoKp * halfex;
		gy += twoKp * halfey;
		gz += twoKp * halfez;
	}
	
	// Integrate rate of change of quaternion
//	gx *= (0.5f * (1.0f / sampleFreq));		// pre-multiply common factors
//	gy *= (0.5f * (1.0f / sampleFreq));
//	gz *= (0.5f * (1.0f / sampleFreq));
	gx *= (0.5f * dt);		// pre-multiply common factors
	gy *= (0.5f * dt);
	gz *= (0.5f * dt);

	qa = q0;
	qb = q1;
	qc = q2;
	q0 += (-qb * gx - qc * gy - q3 * gz);
	q1 += (qa * gx + qc * gz - q3 * gy);
	q2 += (qa * gy - qb * gz + q3 * gx);
	q3 += (qa * gz + qb * gy - qc * gx); 
	
	// Normalise quaternion
	recipNorm = inverseSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
	q0 *= recipNorm;
	q1 *= recipNorm;
	q2 *= recipNorm;
	q3 *= recipNorm;
	
}

void GenericQuat::MadgwickAHRSupdateIMU(float gx, float gy, float gz, float ax, float ay, float az) {
	float recipNorm;
	float s0, s1, s2, s3;
	float qDot1, qDot2, qDot3, qDot4;
	float _2q0, _2q1, _2q2, _2q3, _4q0, _4q1, _4q2 ,_8q1, _8q2, q0q0, q1q1, q2q2, q3q3;

	// Rate of change of quaternion from gyroscope
	qDot1 = 0.5f * (-q1 * gx - q2 * gy - q3 * gz);
	qDot2 = 0.5f * (q0 * gx + q2 * gz - q3 * gy);
	qDot3 = 0.5f * (q0 * gy - q1 * gz + q3 * gx);
	qDot4 = 0.5f * (q0 * gz + q1 * gy - q2 * gx);

	// Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
	if(!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f))) {

		// Normalise accelerometer measurement
		recipNorm = inverseSqrt(ax * ax + ay * ay + az * az);
		ax *= recipNorm;
		ay *= recipNorm;
		az *= recipNorm;   

		// Auxiliary variables to avoid repeated arithmetic
		_2q0 = 2.0f * q0;
		_2q1 = 2.0f * q1;
		_2q2 = 2.0f * q2;
		_2q3 = 2.0f * q3;
		_4q0 = 4.0f * q0;
		_4q1 = 4.0f * q1;
		_4q2 = 4.0f * q2;
		_8q1 = 8.0f * q1;
		_8q2 = 8.0f * q2;
		q0q0 = q0 * q0;
		q1q1 = q1 * q1;
		q2q2 = q2 * q2;
		q3q3 = q3 * q3;

		// Gradient decent algorithm corrective step
		s0 = _4q0 * q2q2 + _2q2 * ax + _4q0 * q1q1 - _2q1 * ay;
		s1 = _4q1 * q3q3 - _2q3 * ax + 4.0f * q0q0 * q1 - _2q0 * ay - _4q1 + _8q1 * q1q1 + _8q1 * q2q2 + _4q1 * az;
		s2 = 4.0f * q0q0 * q2 + _2q0 * ax + _4q2 * q3q3 - _2q3 * ay - _4q2 + _8q2 * q1q1 + _8q2 * q2q2 + _4q2 * az;
		s3 = 4.0f * q1q1 * q3 - _2q1 * ax + 4.0f * q2q2 * q3 - _2q2 * ay;
		recipNorm = inverseSqrt(s0 * s0 + s1 * s1 + s2 * s2 + s3 * s3); // normalise step magnitude
		s0 *= recipNorm;
		s1 *= recipNorm;
		s2 *= recipNorm;
		s3 *= recipNorm;

		// Apply feedback step
		qDot1 -= beta * s0;
		qDot2 -= beta * s1;
		qDot3 -= beta * s2;
		qDot4 -= beta * s3;
	}

	// Integrate rate of change of quaternion to yield quaternion
	q0 += qDot1 * (1.0f / sampleFreq);
	q1 += qDot2 * (1.0f / sampleFreq);
	q2 += qDot3 * (1.0f / sampleFreq);
	q3 += qDot4 * (1.0f / sampleFreq);

	// Normalise quaternion
	recipNorm = inverseSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
	q0 *= recipNorm;
	q1 *= recipNorm;
	q2 *= recipNorm;
	q3 *= recipNorm;
}

float GenericQuat::getRotationAngle(){
	rotationAngle = acos(q0);
	return rotationAngle;
}

float* GenericQuat::getRotationAxe() {
	float f0 = 1/sqrt(1 - q0*q0);
	if (fabs(f0)>0.00001)		
	{
		f0 = 1/f0;
		rotationAxe[0] = q1*f0;
		rotationAxe[1] = q1*f0;		
		rotationAxe[2] = q1*f0;
		
		f0 = inverseSqrt(rotationAxe[0]*rotationAxe[0]+rotationAxe[1]*rotationAxe[1]+rotationAxe[2]*rotationAxe[2]);

		rotationAxe[0] *= f0;
		rotationAxe[1] *= f0;		
		rotationAxe[2] *= f0;		
	} else
	{
		rotationAxe[0] = 0;
		rotationAxe[1] = 0;		
		rotationAxe[2] = 0;		
	}
	
	return rotationAxe;
}

void GenericQuat::update(float* w, float dt) {
	float gx, gy, gz;
	float t = 0.5f*dt;
	
	float qa = q0;
	float qb = q1;
	float qc = q2;
	
	q0 += (-qb*w[0]*t - qc*w[1]*t - q3*w[2]*t);
	q1 += (qa*w[0]*t + qc*w[2]*t - q3*w[1]*t);
	q2 += (qa*w[1]*t - qb*w[2]*t + q3*w[0]*t);
	q3 += (qa*w[2]*t + qb*w[1]*t - qc*w[0]*t); 
	
	// Normalise quaternion
	float recipNorm = inverseSqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3);
	q0 *= recipNorm;
	q1 *= recipNorm;
	q2 *= recipNorm;
	q3 *= recipNorm;
}

void GenericQuat::getEverything(float targetPitchPos, float targetRollPos, float targetYawPos, float t0) {
// ������� ���� �������� � ��������, ��������� �� � �������	
// ����������, ����� t<=0.05

	float t0q0, t0q1, t0q2, t0q3;
	float t1q0, t1q1, t1q2, t1q3;
	float res_q0, res_q1, res_q2, res_q3;	

	t0q0 = targetPitchPos*0.5*degress2rad;
	t0q1 = targetRollPos*0.5*degress2rad;
	t0q2 = targetYawPos*0.5*degress2rad;
	
	float cos_pitch = cos(t0q0);
	float sin_pitch = sin(t0q0);
	float cos_roll = cos(t0q1);
	float sin_roll = sin(t0q1);
	float cos_yaw = cos(t0q2);
	float sin_yaw = sin(t0q2);

	// ��� �������� ���������� (�������� � ������� ����� ������  - targetPitchPos, targetRollPos, targetYawPos) ��������� ��������������� ����������
	// ��������� - ���������� (t0q0, t0q1, t0q2, t0q3)
	t0q0 = cos_pitch*cos_roll*cos_yaw+sin_pitch*sin_roll*sin_yaw;	
	t0q1 = sin_pitch*cos_roll*cos_yaw-cos_pitch*sin_roll*sin_yaw;	
	t0q2 = cos_pitch*sin_roll*cos_yaw+sin_pitch*cos_roll*sin_yaw;	
	t0q3 = cos_pitch*cos_roll*sin_yaw-sin_pitch*sin_roll*cos_yaw;
	
	// � ������� SLERP ��������� ���������� (� ���� �����������), ������� ����� ����� ������� ����������� � ��������� (������������ ������ � �������)  
	// ��������� - ���������� (t1q0, t1q1, t1q2, t1q3)
	float omega = acos(q0*t0q0+q1*t0q1+q2*t0q2+q3*t0q3);
	f_debug[6] = omega*rad2degress;
	
	if (fabs(omega)>0.00001)
	{
		float j = sin(t0*omega)/sin(omega);
		float i = sin((1-t0)*omega)/sin(omega);
		t1q0 = q0*i+t0q0*j;
		t1q1 = q1*i+t0q1*j;
		t1q2 = q2*i+t0q2*j;
		t1q3 = q3*i+t0q3*j;
	} else
	{
		t1q0 = q0*t0+t0q0*(1-t0);
		t1q1 = q1*t0+t0q1*(1-t0);
		t1q2 = q2*t0+t0q2*(1-t0);
		t1q3 = q3*t0+t0q3*(1-t0);
	}

	float f1 = inverseSqrt(t1q0*t1q0 + t1q1*t1q1 + t1q2*t1q2 + t1q3*t1q3);
	t1q0 *= f1;
	t1q1 *= f1;
	t1q2 *= f1;
	t1q3 *= f1;

	// ��������� ���� ��������, ����������� ����� ����������� �� ������� ���������� � ����������� - ���������� (t1q0, t1q1, t1q2, t1q3)
	// ��������� - ���� �������� t_pitch, t_roll, t_yaw
	res_q0 = t1q0*q0+t1q1*q1+t1q2*q2+t1q3*q3;	
	res_q1 = -t1q0*q1+t1q1*q0-t1q2*q3+t1q3*q2;
	res_q2 = -t1q0*q2+t1q1*q3+t1q2*q0-t1q3*q1;
	res_q3 = -t1q0*q3-t1q1*q2+t1q2*q1+t1q3*q0;
	
	t_pitch = atan2(2*(res_q0*res_q1+res_q2*res_q3),res_q0*res_q0-res_q1*res_q1-res_q2*res_q2+res_q3*res_q3);
	t_roll = asin(2*res_q0*res_q2-2*res_q1*res_q3);
	t_yaw = atan2(2*(res_q0*res_q3+res_q1*res_q2),res_q0*res_q0+res_q1*res_q1-res_q2*res_q2-res_q3*res_q3);
	
	t_pitch *= rad2degress;
	t_roll *= rad2degress;
	t_yaw *= rad2degress;	
}