
#include <math.h>
#include "uahrs.h"
#include "pid/pid.h"
#include "../filters_impl.h"

u8 InitState = 0;
double degrees2rads = 0.0174532925f;
double rads2degrees = 57.295779513f;

double   f_debug[9] = { 0.0f, };
double t_pitch, t_roll, t_yaw;
double halfvx, halfvy, halfvz;
double halfex, halfey, halfez;
double dt = 0.002f, inv_dt = 1.0f / 0.002f; // 2 msec

double q0, q1, q2, q3;

// Params:
// accPid3 : Vector3; should contain Accelerometer PID coefficients
void uahrs_init(double* accPid3, double dt)
{
    dt = dt;
    inv_dt = 1.0f / dt;

    q0 = 1;
    q1 = q2 = q3 = 0;

    rotationAngle = 0.0;
    rotationAxis[0] = 0.0;
    rotationAxis[1] = 0.0;
    rotationAxis[2] = 0.0;

    _prev_q0 = 1;
    _prev_q1 = _prev_q2 = _prev_q3 = 0;

    InitState = 0;

    integralFBx = 0.0f;
    integralFBy = 0.0f;
    integralFBz = 0.0f;

    acc2Kp = 2.0f*accPid3[0];			// 2 * proportional gain (Kp)
    acc2Ki = 2.0f*accPid3[1];			// 2 * integral gain (Ki)
    acc2Kd = 2.0f*accPid3[2];			// 2 * differential gain (Kd)
}

void setInitState_byAngles(double* euler_angles) {
    double cos_pitch = cos(euler_angles[PITCH] * 0.5);
    double sin_pitch = sin(euler_angles[PITCH] * 0.5);
    double cos_roll = cos(euler_angles[ROLL] * 0.5);
    double sin_roll = sin(euler_angles[ROLL] * 0.5);
    double cos_yaw = cos(euler_angles[YAW] * 0.5);
    double sin_yaw = sin(euler_angles[YAW] * 0.5);

    q0 = cos_pitch*cos_roll*cos_yaw + sin_pitch*sin_roll*sin_yaw;
    q1 = sin_pitch*cos_roll*cos_yaw - cos_pitch*sin_roll*sin_yaw;
    q2 = cos_pitch*sin_roll*cos_yaw + sin_pitch*cos_roll*sin_yaw;
    q3 = cos_pitch*cos_roll*sin_yaw - sin_pitch*sin_roll*cos_yaw;
}

void setInitState_byAccMag(s16* acc, s16* mag) {
	double accScaleX = v_get_val(_kf6_3Params[1].MeasScale, 0);
	double accDriftX = v_get_val(_kf6_3Params[1].MeasDrift, 0);
	double accScaleY = v_get_val(_kf6_3Params[1].MeasScale, 1);
	double accDriftY = v_get_val(_kf6_3Params[1].MeasDrift, 1);
	double accScaleZ = v_get_val(_kf6_3Params[1].MeasScale, 2);
	double accDriftZ = v_get_val(_kf6_3Params[1].MeasDrift, 2);

	double magScaleX = v_get_val(_kf6_3Params[2].MeasScale, 0);
	double magDriftX = v_get_val(_kf6_3Params[2].MeasDrift, 0);
	double magScaleY = v_get_val(_kf6_3Params[2].MeasScale, 1);
	double magDriftY = v_get_val(_kf6_3Params[2].MeasDrift, 1);
	double magScaleZ = v_get_val(_kf6_3Params[2].MeasScale, 2);
	double magDriftZ = v_get_val(_kf6_3Params[2].MeasDrift, 2);


    double angles[3] = { 0.0f, };
	double realAcc[3] = { acc[0] * accScaleX - accDriftX, acc[1] * accScaleY - accDriftY, acc[2] * accScaleZ - accDriftZ };
	double realMag[3] = { mag[0] * magScaleX - magDriftX, mag[1] * magScaleY - magDriftY, mag[2] * magScaleZ - magDriftZ };
    float t = (float)(realAcc[0] * realAcc[0] + realAcc[1] * realAcc[1] + realAcc[2] * realAcc[2]);
    t = FastInvSqrt(t);
    double x, y, z;
    x = -realAcc[0] * t;
    y = -realAcc[1] * t;
    z = -realAcc[2] * t;

    angles[PITCH] = atan2(y, z);
    angles[ROLL] = atan2(-x, sqrt(y*y + z*z));

    if (mag) {
        t = FastInvSqrt((fp32)(realMag[0] * realMag[0] + realMag[1] * realMag[1] + realMag[2] * realMag[2]));
        x = realMag[ROLL] * t;
        y = realMag[PITCH] * t;
        z = realMag[YAW] * t;

        double X = y*cos(-angles[PITCH]) + x*sin(-angles[PITCH])*sin(angles[ROLL]) + z*sin(-angles[PITCH])*cos(angles[ROLL]);
        double Y = ((x*cos(angles[ROLL])) - z*sin(angles[ROLL]));
        angles[YAW] = atan2(Y, X);
    }
    else
        angles[YAW] = 0;
    setInitState_byAngles(angles);
    InitState = 1;
}


void uahrs_MahonyAHRSupdateIMU(double gx, double gy, double gz, double ax, double ay, double az) {
    float recipNorm;
    double qa, qb, qc;
    double prev_halfex, prev_halfey, prev_halfez, delta;
    double fbX = 0.0f, fbY = 0.0f, fbZ = 0.0f;

    // Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
    if (!((ax == 0.0f) && (ay == 0.0f) && (az == 0.0f))) {

        // Normalise accelerometer measurement
        recipNorm = FastInvSqrt(ax * ax + ay * ay + az * az);
        ax *= recipNorm;
        ay *= recipNorm;
        az *= recipNorm;

        // Estimated direction of gravity and vector perpendicular to magnetic flux
        halfvx = q1 * q3 - q0 * q2;
        halfvy = q0 * q1 + q2 * q3;
        halfvz = q0 * q0 - 0.5f + q3 * q3;

        // Caching previous error
        prev_halfex = halfex;
        prev_halfey = halfey;
        prev_halfez = halfez;

		// Error is sum of cross product between estimated and measured direction of gravity
        halfex = (ay * halfvz - az * halfvy);
        halfey = (az * halfvx - ax * halfvz);
        halfez = (ax * halfvy - ay * halfvx);

        // Compute and apply integral feedback if enabled
        if (fabs(acc2Ki) > 0.0f) {
            integralFBx += acc2Ki * halfex * dt;	// integral error scaled by Ki
            integralFBy += acc2Ki * halfey * dt;
            integralFBz += acc2Ki * halfez * dt;

            fbX += integralFBx;	// apply integral feedback
            fbY += integralFBy;
            fbZ += integralFBz;
        }
        else {
            integralFBx = 0.0f;	// prevent integral windup
            integralFBy = 0.0f;
            integralFBz = 0.0f;
        }

        if (fabs(acc2Kd) > 0.0f)
        {
            // FIXME: add caching var for acc2Kd * inv_dt to reduce multiplications count
            delta = halfex - prev_halfex;
            fbX += acc2Kd * delta * inv_dt;	// apply differential feedback

            delta = halfey - prev_halfey;
            fbY += acc2Kd * delta * inv_dt;

            delta = halfez - prev_halfez;
            fbZ += acc2Kd * delta * inv_dt;
        }

        // Apply proportional feedback
        // Industrial (standard) form of PID is used
        gx += acc2Kp * (halfex + fbX);
        gy += acc2Kp * (halfey + fbY);
        gz += acc2Kp * (halfez + fbZ);
    }

    // Integrate rate of change of quaternion
    gx *= (0.5f * dt);		// pre-multiply common factors
    gy *= (0.5f * dt);
    gz *= (0.5f * dt);

    qa = q0;
    qb = q1;
    qc = q2;
    q0 += (-qb * gx - qc * gy - q3 * gz);
    q1 += (qa * gx + qc * gz - q3 * gy);
    q2 += (qa * gy - qb * gz + q3 * gx);
    q3 += (qa * gz + qb * gy - qc * gx);

    // Normalise quaternion
    recipNorm = FastInvSqrt(q0 * q0 + q1 * q1 + q2 * q2 + q3 * q3);
    q0 *= recipNorm;
    q1 *= recipNorm;
    q2 *= recipNorm;
    q3 *= recipNorm;

}

void uahrs_getEverything(double targetPitchPos, double targetRollPos, double targetYawPos, double t0) {
    // ������� ���� �������� � ��������, ��������� �� � �������	
    // ����������, ����� t<=0.05

    double t0q0, t0q1, t0q2, t0q3;
    double t1q0, t1q1, t1q2, t1q3;
    double res_q0, res_q1, res_q2, res_q3;

    t0q0 = targetPitchPos*0.5*degrees2rads;
    t0q1 = targetRollPos*0.5*degrees2rads;
    t0q2 = targetYawPos*0.5*degrees2rads;

    double cos_pitch = cos(t0q0);
    double sin_pitch = sin(t0q0);
    double cos_roll = cos(t0q1);
    double sin_roll = sin(t0q1);
    double cos_yaw = cos(t0q2);
    double sin_yaw = sin(t0q2);

    // ��� �������� ���������� (�������� � ������� ����� ������  - targetPitchPos, targetRollPos, targetYawPos) ��������� ��������������� ����������
    // ��������� - ���������� (t0q0, t0q1, t0q2, t0q3)
    t0q0 = cos_pitch*cos_roll*cos_yaw + sin_pitch*sin_roll*sin_yaw;
    t0q1 = sin_pitch*cos_roll*cos_yaw - cos_pitch*sin_roll*sin_yaw;
    t0q2 = cos_pitch*sin_roll*cos_yaw + sin_pitch*cos_roll*sin_yaw;
    t0q3 = cos_pitch*cos_roll*sin_yaw - sin_pitch*sin_roll*cos_yaw;

    // � ������� SLERP ��������� ���������� (� ���� �����������), ������� ����� ����� ������� ����������� � ��������� (������������ ������ � �������)  
    // ��������� - ���������� (t1q0, t1q1, t1q2, t1q3)
    double omega = acos(q0*t0q0 + q1*t0q1 + q2*t0q2 + q3*t0q3);
    f_debug[6] = omega*rads2degrees;

    if (fabs(omega) > 0.00001)
    {
        double j = sin(t0*omega) / sin(omega);
        double i = sin((1 - t0)*omega) / sin(omega);
        t1q0 = q0*i + t0q0*j;
        t1q1 = q1*i + t0q1*j;
        t1q2 = q2*i + t0q2*j;
        t1q3 = q3*i + t0q3*j;
    }
    else
    {
        t1q0 = q0*t0 + t0q0*(1 - t0);
        t1q1 = q1*t0 + t0q1*(1 - t0);
        t1q2 = q2*t0 + t0q2*(1 - t0);
        t1q3 = q3*t0 + t0q3*(1 - t0);
    }

    float f1 = FastInvSqrt(t1q0*t1q0 + t1q1*t1q1 + t1q2*t1q2 + t1q3*t1q3);
    t1q0 *= f1;
    t1q1 *= f1;
    t1q2 *= f1;
    t1q3 *= f1;

    // ��������� ���� ��������, ����������� ����� ����������� �� ������� ���������� � ����������� - ���������� (t1q0, t1q1, t1q2, t1q3)
    // ��������� - ���� �������� t_pitch, t_roll, t_yaw
    res_q0 = t1q0*q0 + t1q1*q1 + t1q2*q2 + t1q3*q3;
    res_q1 = -t1q0*q1 + t1q1*q0 - t1q2*q3 + t1q3*q2;
    res_q2 = -t1q0*q2 + t1q1*q3 + t1q2*q0 - t1q3*q1;
    res_q3 = -t1q0*q3 - t1q1*q2 + t1q2*q1 + t1q3*q0;

    t_pitch = atan2(2 * (res_q0*res_q1 + res_q2*res_q3), res_q0*res_q0 - res_q1*res_q1 - res_q2*res_q2 + res_q3*res_q3);
    t_roll = asin(2 * res_q0*res_q2 - 2 * res_q1*res_q3);
    t_yaw = atan2(2 * (res_q0*res_q3 + res_q1*res_q2), res_q0*res_q0 + res_q1*res_q1 - res_q2*res_q2 - res_q3*res_q3);

    t_pitch *= rads2degrees;
    t_roll *= rads2degrees;
    t_yaw *= rads2degrees;
}

void uahrs_getEulerAngles(double* euler_angles) {
    euler_angles[PITCH] = atan2(2 * (q0*q1 + q2*q3), q0*q0 - q1*q1 - q2*q2 + q3*q3) * rads2degrees;
    euler_angles[ROLL] = asin(2 * q0*q2 - 2 * q1*q3) * rads2degrees;
    //euler_angles[ROLL] = atan2(2 * (q0*q1 + q2*q3), q0*q0 - q1*q1 - q2*q2 + q3*q3) * rads2degrees;
    //euler_angles[PITCH] = asin(2 * q0*q2 - 2 * q1*q3) * rads2degrees;
    euler_angles[YAW] = atan2(2 * (q0*q3 + q1*q2), q0*q0 + q1*q1 - q2*q2 - q3*q3) * rads2degrees;
}

void uahrs_getDCM() {
    _e00 = q0*q0 + q1*q1 - q2*q2 - q3*q3;
    _e01 = 2.0*(q1*q2 - q0*q3);
    _e02 = 2.0*(q1*q3 + q0*q2);
    _e10 = 2.0*(q1*q2 + q0*q3);
    _e11 = q0*q0 - q1*q1 + q2*q2 - q3*q3;
    _e12 = 2.0*(q2*q3 - q0*q1);
    _e20 = 2.0*(q1*q3 - q0*q2);
    _e21 = 2.0*(q2*q3 + q0*q1);
    _e22 = q0*q0 - q1*q1 - q2*q2 + q3*q3;
}

double uahrs_getRotationAngle() {
    rotationAngle = acos(q0);
    return rotationAngle;
}

double* uahrs_getRotationAxis() {
    //double f0 = 1 / sqrt(1 - q0*q0);
    double f0 = sqrt(1 - q0*q0);
    if (fabs(f0) < 0.99999)
    {
        //f0 = 1 / f0;
        rotationAxis[0] = q1*f0;
        rotationAxis[1] = q1*f0;
        rotationAxis[2] = q1*f0;

        f0 = FastInvSqrt(rotationAxis[0] * rotationAxis[0] + rotationAxis[1] * rotationAxis[1] + rotationAxis[2] * rotationAxis[2]);

        rotationAxis[0] *= f0;
        rotationAxis[1] *= f0;
        rotationAxis[2] *= f0;
    }
    else
    {
        rotationAxis[0] = 0;
        rotationAxis[1] = 0;
        rotationAxis[2] = 0;
    }

    return rotationAxis;
}

void uahrs_update(double* w) {
    //double gx, gy, gz;
    double t = 0.5f*dt;

    double qa = q0;
    double qb = q1;
    double qc = q2;

    q0 += (-qb*w[0] - qc*w[1] - q3*w[2]) * t;
    q1 += ( qa*w[0] + qc*w[2] - q3*w[1]) * t;
    q2 += ( qa*w[1] - qb*w[2] + q3*w[0]) * t;
    q3 += ( qa*w[2] + qb*w[1] - qc*w[0]) * t;

    // Normalise quaternion
    double recipNorm = FastInvSqrt(q0*q0 + q1*q1 + q2*q2 + q3*q3);
    q0 *= recipNorm;
    q1 *= recipNorm;
    q2 *= recipNorm;
    q3 *= recipNorm;
}

//---------------------------------------------------------------------------------------------------
// Fast inverse square-root
// See: https://pizer.wordpress.com/2008/10/12/fast-inverse-square-root/

fp32 FastInvSqrt(fp32 x) {
    /*u64 i = 0x5F1F1412 - (*(u64*)&x >> 1);
    fp32 tmp = *(fp32*)&i;
    fp32 y = tmp * (1.69000231f - 0.714158168f * x * tmp * tmp);
    return y;*/
    long i;
    float x2, y;
    const float threehalfs = 1.5F;

    x2 = x * 0.5F;
    y = x;
    i = *(long *)&y;                       // evil floating point bit level hacking
    i = 0x5f3759df - (i >> 1);               // what the fuck? 
    y = *(float *)&i;
    y = y * (threehalfs - (x2 * y * y));   // 1st iteration
    y  = y * ( threehalfs - ( x2 * y * y ) );   // 2nd iteration, this can be removed

    return y;
}