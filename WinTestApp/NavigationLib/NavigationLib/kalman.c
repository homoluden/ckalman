#include "kalman.h"

KFParams _kf6_3Params[3];
KFState _kf6_3State[3];

extern void KF_Step(KFParams *p, KFState *s)
{
	static VEC *xProp = VNULL, *KTermZ = VNULL, *KTermX = VNULL;
	static MAT *pProp = MNULL, *pProp2 = MNULL, *K = MNULL, *K2 = MNULL, *ekh = MNULL, *kDenom = MNULL, *kDenom2 = MNULL;

	xProp = v_resize(xProp, s->X->dim);
	pProp = m_resize(pProp, p->pCov->m, p->pCov->n);
	pProp2 = m_resize(pProp2, p->pCov->m, p->pCov->n);
	K = m_resize(K, p->pCov->m, p->mCov->n);
	K2 = m_resize(K2, p->pCov->m, p->mCov->n);
	KTermZ = v_resize(KTermZ, s->Z->dim);
	KTermX = v_resize(KTermX, s->X->dim);
	ekh = m_resize(ekh, p->e->m, p->e->n);
	kDenom = m_resize(kDenom, p->mCov->m, p->pCov->n);
	kDenom2 = m_resize(kDenom2, p->mCov->m, p->mCov->n);

	// WARN: Don't use in m_mlt the same pointer for one of argument AND for out storage
	// pProp = f * p * ft + pCov;
	pProp = m_mlt(p->F, s->P, pProp);
	pProp2 = m_mlt(pProp, p->Ft, pProp2);
	pProp2 = m_add(pProp2, p->pCov, pProp2);

	// k = pProp * ht / (h * pProp * ht + mCov);
	kDenom = m_mlt(p->H, pProp2, kDenom);
	kDenom2 = m_mlt(kDenom, p->Ht, kDenom2);
	kDenom2 = m_add(kDenom2, p->mCov, kDenom2);
	kDenom = m_resize(kDenom, p->mCov->m, p->mCov->n);
	kDenom = m_inverse(kDenom2, kDenom);

	K = m_mlt(pProp2, p->Ht, K);

	K2 = m_mlt(K, kDenom, K2);

	// ekh = e - k * h;
	ekh = m_mlt(K2, p->H, ekh);
	ekh = sm_mlt(-1.0, ekh, ekh);
	ekh = m_add(p->e, ekh, ekh);

	// p = ekh * pProp;
	s->P = m_mlt(ekh, pProp, s->P);

	// xProp = f * x;
	xProp = mv_mlt(p->F, s->X, xProp);

	// x = xProp + k * (Z - h * xProp);
	KTermZ = mv_mltadd(s->Z, xProp, p->H, -1.0, KTermZ);
	KTermX = mv_mlt(K2, KTermZ, KTermX);
	s->X = v_add(xProp, KTermX, s->X);

	// Zest = h * x;
	s->Zest = mv_mlt(p->H, s->X, s->Zest);
}