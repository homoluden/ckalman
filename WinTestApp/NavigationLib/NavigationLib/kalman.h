#ifndef __KALMAN_H
#define __KALMAN_H

//#define REAL_FLT

#include "matrix2.h"

typedef struct 
{
	MAT *F, *Ft, *H, *Ht, *mCov, *pCov, *e;
	VEC *MeasDrift, *MeasScale;
} KFParams;

typedef struct 
{
	VEC *X, *Z, *Zest;
	MAT *P;
} KFState;

extern void KF_Step(KFParams *params, KFState *state);

#endif
