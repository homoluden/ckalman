﻿using System;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;     // DLL support

namespace NavigationConsole
{
    class Program
    {
        [DllImport("NavigationLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Stage0_SeedParams(Int64[] params39, Byte idx);

        [DllImport("NavigationLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void Stage1_OptimizeGyro(short[] inputs, UInt32 rows, UInt32 cols, Int64[] params39);

        [DllImport("NavigationLib.dll", CallingConvention = CallingConvention.Cdecl)]
        public static extern void KF6_3_Init(Int64[] params39, Byte idx);

        [DllImport("NavigationLib.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void KF6_3_Step(short[] inpInt, double[] est, Byte idx);

        [DllImport("NavigationLib.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void UAHRS_Init(short[] initAcc3, short[] initMag3, Int64[] accPid3, double dt);

        [DllImport("NavigationLib.dll", CallingConvention = CallingConvention.Cdecl)]
        static extern void UAHRS_Step(double[] gyro, double[] acc, double[] eulerResult);

        static void Main()
        {
            Console.WriteLine("Kalman Filter Test");

            string fileContent;// = File.ReadAllText("data.log");
            using (StreamReader reader = new StreamReader("data.log"))
            {
                fileContent = reader.ReadToEnd();
            }

            var rows = fileContent.Split("\n\r".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

            var inputs = new short[rows.Length, 10];

            byte iroll = 2, ipitch = 1, iyaw = 3;

            //var gyroScale = 250.0 / 32768.0;
            //var accelScale = 2.0 / 32768.0;
            //var magScale = 10.0 * 4219.0 / 32768.0;

            // Accelerometer PID coefficients
            //                        Kp,       Ki,        Kd
            var accPid = new Int64[] { 0, 0, 0 };//{ -17010000, 253000000, 600000 };

            var inp = new short[3];

            for (Int32 i = 0; i < rows.Length; i++)
            {
                var row = rows[i];
                var vals = row.Split(" \t".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);

                // vals[0] - Time
                inputs[i, 0] = short.Parse(vals[0]);
                inputs[i, 1] = (short)(-(short.Parse(vals[ipitch]))); // Sensor gives ROLL - PITCH - YAW
                inputs[i, 2] = (short)(-(short.Parse(vals[iroll]))); // We need it in other order (PITCH - ROLL - YAW)
                inputs[i, 3] = short.Parse(vals[iyaw]);
                inputs[i, 4] = (short)(-(short.Parse(vals[ipitch + 3])));
                inputs[i, 5] = (short)(-(short.Parse(vals[iroll + 3])));
                inputs[i, 6] = short.Parse(vals[iyaw + 3]);
                inputs[i, 7] = (short.Parse(vals[8]));
                inputs[i, 8] = (short.Parse(vals[7]));
                inputs[i, 9] = (short.Parse(vals[9]));
            }
            
            var parameters = new Int64[39];

            Stage0_SeedParams(parameters, (Byte)1);
            KF6_3_Init(parameters, (Byte)1);

            Stage0_SeedParams(parameters, (Byte)2);
            KF6_3_Init(parameters, (Byte)2);

            var gyroOut = new double[3];
            var accelOut = new double[3];
            var magOut = new double[3];

            var initAcc = new Int64[] { 0, 0, 0 };
            var initMag = new Int64[] { 0, 0, 0 };
            var n = 500;

            for (Int32 i = 0; i < n; i++)
            {
                initAcc[0] += inputs[i, 4]; // Sensor gives ROLL - PITCH - YAW
                initAcc[1] += inputs[i, 5];  // We need it in other order (PITCH - ROLL - YAW)
                initAcc[2] += inputs[i, 6];   // Inverting Z axis due to Coordinate System rotation

                initMag[0] += inputs[i, 7];
                initMag[1] += inputs[i, 8];
                initMag[2] += inputs[i, 9];   // Inverting Z axis due to Coordinate System rotation

                // Let's warm up KF's to avoid zero Init State
                //inp[0] = inputs[i, 1]; // Sensor gives ROLL - PITCH - YAW
                //inp[1] = inputs[i, 2];  // We need it in other order (PITCH - ROLL - YAW)
                //inp[2] = inputs[i, 3];  // Inverting Z axis due to Coordinate System rotation

                //KF6_3_Step(inp, gyroOut, 0);

                inp[0] = inputs[i, 4];
                inp[1] = inputs[i, 5];
                inp[2] = inputs[i, 6];  // Inverting Z axis due to Coordinate System rotation

                KF6_3_Step(inp, accelOut, 1);

                inp[0] = inputs[i, 7];
                inp[1] = inputs[i, 8];
                inp[2] = inputs[i, 9];  // Inverting Z axis due to Coordinate System rotation

                KF6_3_Step(inp, magOut, 2);
            }

            // Calculatin avarages to use in Initial State calculation.
            // Assuming that first N measurements was takken when object was relaxed (no motion).
            var acc = new short[3];
            var mag = new short[3];

            acc[0] = (short)(initAcc[0] / n);
            acc[1] = (short)(initAcc[1] / n);
            acc[2] = (short)(initAcc[2] / n);

            mag[0] = (short)(initMag[0] / n);
            mag[1] = (short)(initMag[1] / n);
            mag[2] = (short)(initMag[2] / n);

            UAHRS_Init(acc, mag, accPid, 0.002f);

            var flatInputs = new short[inputs.Length];
            //inputs.CopyTo(flatInputs, 0);
            Buffer.BlockCopy(inputs, 0, flatInputs, 0, 2 * 10 * rows.Length);

            Stage0_SeedParams(parameters, (Byte)0);
            Stage0_GiroDriftCalibrtion(flatInputs, (UInt32)rows.Length, parameters);

            KF6_3_Init(parameters, (Byte)0);

            FilteringSim(inputs, rows.Length, "filtered.log");

            Console.WriteLine("\n\nPress Enter...");
            Console.ReadLine();
        }

        static void Stage0_GiroDriftCalibrtion(short[] inputs, UInt32 rowsCount, Int64[] parameters)
        {
            Stage1_OptimizeGyro(inputs, rowsCount, 10, parameters);
        }

        static void FilteringSim(short[,] inputs, Int32 length, string writeTo)
        {

            //var gyroScale = 250.0 / 32768.0;
            //var accelScale = 2.0 / 32768.0;
            //var magScale = 10.0 * 4219.0 / 32768.0;

            // Accelerometer PID coefficients
            //                        Kp,       Ki,        Kd
            var accPid = new Int64[] { 0, 0, 0 };//{ -17010000, 253000000, 600000 };

            var inp = new short[3];

            var gyroOut = new double[3];
            var accelOut = new double[3];
            var magOut = new double[3];
            var eulerOut = new double[3];

            using (var file = File.Create(writeTo))
            {
                using (var writer = new StreamWriter(file))
                {
                    for (Int32 i = 0; i < length; i++)
                    {
                        var t = inputs[i, 0] * 0.002;

                        inp[0] = inputs[i, 1]; // Sensor gives ROLL - PITCH - YAW
                        inp[1] = inputs[i, 2];  // We need it in other order (PITCH - ROLL - YAW)
                        inp[2] = inputs[i, 3];  // Inverting Z axis due to Coordinate System rotation

                        KF6_3_Step(inp, gyroOut, 0);

                        inp[0] = inputs[i, 4];
                        inp[1] = inputs[i, 5];
                        inp[2] = inputs[i, 6];  // Inverting Z axis due to Coordinate System rotation

                        KF6_3_Step(inp, accelOut, 1);

                        inp[0] = inputs[i, 7];
                        inp[1] = inputs[i, 8];
                        inp[2] = inputs[i, 9];  // Inverting Z axis due to Coordinate System rotation

                        KF6_3_Step(inp, magOut, 2);

                        UAHRS_Step(gyroOut, accelOut, eulerOut);

                        var txt = string.Format(
                            "{0:0.00000}  {1:0.00000}  {2:0.00000}  {3:0.00000}  {4:0.00000}  {5:0.00000}  {6:0.00000}  {7:0.00000}  {8:0.00000}  {9:0.00000}  {10:0.00000}  {11:0.00000}  {12:0.00000}",
                            t, gyroOut[0], gyroOut[1], gyroOut[2], accelOut[0], accelOut[1], accelOut[2], magOut[0], magOut[1], magOut[2], eulerOut[0], eulerOut[1], eulerOut[2]);
                        writer.WriteLine(txt);
                        Console.Write("\r{0:0.00000} {1:0.00000} {2:0.00000}", eulerOut[0], eulerOut[1], eulerOut[2]);
                    }

                }
            }
        }
    }
}
