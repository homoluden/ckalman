#ifndef __MAIN_H
#define __MAIN_H

#include "stm32f4xx_hal.h"
#include "typedef.h"

extern I2C_HandleTypeDef hi2c1;
extern I2C_HandleTypeDef hi2c2;

extern SPI_HandleTypeDef hspi1;
extern SPI_HandleTypeDef hspi3;

extern UART_HandleTypeDef huart1;
extern USART_HandleTypeDef husart2;
extern USART_HandleTypeDef husart3;
extern USART_HandleTypeDef husart6;

extern void U3_Tx(u8 *data, u16 size);

#endif