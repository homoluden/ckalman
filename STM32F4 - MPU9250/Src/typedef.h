#ifndef __TYPEDEF_H
#define __TYPEDEF_H

#define u8 uint8_t
#define u16 unsigned short
#define s8 int8_t
#define s16 signed short
#define u32 unsigned int
#define fp32 float

typedef struct {
  u16 T;
  s16 data[3];
} s16_MEAS3;

#endif