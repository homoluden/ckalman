/*====================================================================================================*/
/*====================================================================================================*/
#include "stm32f4xx.h"
#include "module_imu.h"
#include "module_mpu9250.h"
/*====================================================================================================*/
/*====================================================================================================*/
static s16 Byte16(u8 LSB, u8 MSB);
/*====================================================================================================*/
/*====================================================================================================*/
SensorAcc Acc = {0};
SensorGyr Gyr = {0};

#ifdef USE_SENSOR_MAG
SensorMag Mag = {0};
#endif

#ifdef USE_SENSOR_TEMP
SensorTemp Temp = {0};
#endif

#ifdef USE_SENSOR_BARO
SensorBaro Baro = {0};
#endif
/*====================================================================================================*/
/*====================================================================================================*
**函數 : Sensor_Init
**功能 : 初始化 Sensor
**輸入 : None
**輸出 : None
**使用 : Sensor_Init();
**====================================================================================================*/
/*====================================================================================================*/
void Sensor_Init( void )
{
//  MPU_InitTypeDef MPU_InitStruct;

  Acc.X = 0;
  Acc.Y = 0;
  Acc.Z = 0;
  Acc.OffsetX = 0;//+7;
  Acc.OffsetY = 0;//+146;
  Acc.OffsetZ = 0;//-291;
  Acc.TrueX = 0.0f;
  Acc.TrueY = 0.0f;
  Acc.TrueZ = 0.0f;

  Gyr.X = 0;
  Gyr.Y = 0;
  Gyr.Z = 0;
  Gyr.OffsetX = 0;
  Gyr.OffsetY = 0;
  Gyr.OffsetZ = 0;
  Gyr.TrueX = 0.0f;
  Gyr.TrueY = 0.0f;
  Gyr.TrueZ = 0.0f;

#ifdef USE_SENSOR_MAG
  Mag.X = 0;
  Mag.Y = 0;
  Mag.Z = 0;
  Mag.AdjustX = 0;
  Mag.AdjustY = 0;
  Mag.AdjustZ = 0;
  Mag.TrueX = 0.0f;
  Mag.TrueY = 0.0f;
  Mag.TrueZ = 0.0f;
#endif

#ifdef USE_SENSOR_TEMP
  Temp.T = 0;
  Temp.OffsetT = TEMP_OFFSET;
  Temp.TrueT = 0.0f;
#endif

#ifdef USE_SENSOR_BARO
  Baro.Temp = 0.0f;
  Baro.Press = 0.0f;
  Baro.Height = 0.0f;
#endif

//  MPU_InitStruct.MPU_LowPassFilter = MPU_LPS_42Hz;
//  MPU_InitStruct.MPU_Acc_FullScale = MPU_AccFS_4g;
//  MPU_InitStruct.MPU_Gyr_FullScale = MPU_GyrFS_2000dps;
//  MPU9150_Init(&MPU_InitStruct);
  MPU9250_Init();
  while(MPU9250_Check() != SUCCESS);

#ifdef USE_SENSOR_BARO
  MS5611_Init();
#endif
}
/*====================================================================================================*/
/*====================================================================================================*
**函數 : Sensor_Read
**功能 : Sensor Read
**輸入 : ReadBuf
**輸出 : None
**使用 : Sensor_Read(SampleRateFreg);
**====================================================================================================*/
/*====================================================================================================*/
void Sensor_Read(void)
{
  u8 ReadBuf[20] = {0};

#ifdef USE_SENSOR_BARO
  static s8 ReadCount = 0;
  static s32 Baro_Buf[2] = {0}; // 沒加 static 資料會有問題
#endif

  MPU9250_Read(ReadBuf);

  Acc.X  = Byte16(ReadBuf[0],  ReadBuf[1]);  // Acc.X
  Acc.Y  = Byte16(ReadBuf[2],  ReadBuf[3]);  // Acc.Y
  Acc.Z  = Byte16(ReadBuf[4],  ReadBuf[5]);  // Acc.Z
  Gyr.X  = Byte16(ReadBuf[8],  ReadBuf[9]);  // Gyr.X
  Gyr.Y  = Byte16(ReadBuf[10], ReadBuf[11]); // Gyr.Y
  Gyr.Z  = Byte16(ReadBuf[12], ReadBuf[13]); // Gyr.Z

#ifdef USE_SENSOR_MAG
  Mag.Z  = Byte16(ReadBuf[14], ReadBuf[15]); // Mag.X
  Mag.Z  = Byte16(ReadBuf[16], ReadBuf[17]); // Mag.Y
  Mag.Z  = Byte16(ReadBuf[18], ReadBuf[19]); // Mag.Z
#endif

#ifdef USE_SENSOR_TEMP
  Temp.T = Byte16(ReadBuf[6],  ReadBuf[7]);  // Temp
#endif
}
/*====================================================================================================*/
/*====================================================================================================*/
static s16 Byte16(u8 LSB, u8 MSB)
{
  return MSB<<8 | LSB;
}