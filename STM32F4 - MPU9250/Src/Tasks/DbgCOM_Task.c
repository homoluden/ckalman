#include "stm32f4xx_hal.h"
#include "cmsis_os.h"
#include "../typedef.h"
#include "../main.h"
#include "DbgCOM_Task.h"
#include "../MPU9250/MPU9250.h"

extern osMessageQId gyroQueueHandle;

char const *fmt = "%ld\t%hd\t%hd\t%hd\r\n";
char msg[100] = {0,};

void usbCOM_Task(void const * argument)
{
  TickType_t xLastWakeTime = xTaskGetTickCount();
  u16 lng = 0;
  s16_MEAS3 single_meas = {0,};
  
  for(;;)
  {
    // TODO: get msg from MessageQ
    if( xQueueReceive( gyroQueueHandle, &( single_meas ), ( TickType_t ) 10 ) )
    {
      lng = sprintf( msg, fmt, single_meas.T, single_meas.data[0], single_meas.data[1], single_meas.data[2] );
      U3_Tx((u8 *)msg, lng);
    }
    
    vTaskDelayUntil(&xLastWakeTime, 1500);
  }
}